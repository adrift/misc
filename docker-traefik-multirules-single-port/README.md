i couldn't remember, so here's a quick test to see if multiple "frontend" traefik rules can be served by the same `traefik.port` and `traefik.frontend.entryPoints` labels

tl;dr: yep

###quick start

```
docker-compose up && docker-compose rm -f
```

enable testing in your browser by aliasing the (docker) IP of the traefik container to the hostname listed in the `docker-compose.yml` example below. make sure to remove this when you're done!

```
echo "172.16.0.2 multiple-traefik-rules.adrift.io" | sudo tee -a /etc/hosts
```

then simply try to hit one of the paths defined in that `docker-compose.yml` labels
* multiple-traefik-rules.adrift.io/whoami1
* multiple-traefik-rules.adrift.io/whoami2
